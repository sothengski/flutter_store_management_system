import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:store_management/data.dart' as Data;

void Login(
  Function function,
  String email,
  String password,
) async {
  var jsonBody = {
    "email": email,
    "password": password,
  };

  var response = await http.post(
    "http://localhost:8000/api/login",
    headers: {
      HttpHeaders.contentTypeHeader: "application/x-www-form-urlencoded",
      "X-Requested-With": "XMLHttpRequest",
    },
    body: jsonBody,
  );
  var result = json.decode(response.body);

  Data.storeId = "${result["store"][0]["id"]}";
  Data.token = "${result["access_token"]}";

  function();
}
