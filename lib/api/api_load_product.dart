import 'dart:convert';
import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:store_management/model/product_model.dart';
import 'package:store_management/data.dart' as Data;

final String token = Data.token;
final String storeId = Data.storeId;
// final String categoryId = Data.categoryId;

Future<List<Product>> getAllProduct() async {
  List<Product> productList = [];
  var jsonBody = {
    "store_id": storeId,
  };

  var response = await http.post(
    "http://localhost:8000/api/product/showAll",
    headers: {
      HttpHeaders.contentTypeHeader: "application/x-www-form-urlencoded",
      "X-Requested-With": "XMLHttpRequest",
      HttpHeaders.authorizationHeader: "Bearer $token",
    },
    body: jsonBody,
  );
  var result = json.decode(response.body);
  List<dynamic> datas = result["data"];

  if (datas != null) {
    for (int i = 0; i < datas.length; i++) {
      productList.add(Product.fromJson(datas[i]));
    }
  }

  return productList;
}

void addProduct(Function popFunction, name, categoryId, amount, priceOut, unit,
    priceIn) async {
  final Map<String, String> jsonBody = {
    "store_id": storeId,
    "name": "$name",
    "category_id": "$categoryId",
    "amount": "$amount",
    "price_out": "$priceOut",
    "measure_unit": "$unit",
    "price_in": "$priceIn"
  };

  var response = await http.post(
    "http://localhost:8000/api/product",
    headers: {HttpHeaders.authorizationHeader: "Bearer $token"},
    body: jsonBody,
  );

  var result = json.decode(response.body);

  if (result["response"] == 1) {
    popFunction();
  }
}
