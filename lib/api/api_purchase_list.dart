import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:store_management/model/purchase_model.dart';
import 'package:store_management/data.dart' as Data;

final String token = Data.token;
final String storeId = Data.storeId;

Future<List<Purchase>> getAllPurchaseList() async {
  List<Purchase> purchaseList = [];

  final Map<String, String> jsonBody = {
    "store_id": storeId,
  };

  var response = await http.post(
    "http://localhost:8000/api/transaction/showPurchaseList",
    headers: {
      HttpHeaders.contentTypeHeader: "application/x-www-form-urlencoded",
      "X-Requested-With": "XMLHttpRequest",
      HttpHeaders.authorizationHeader: "Bearer $token",
    },
    body: jsonBody,
  );

  var result = json.decode(response.body);

  List<dynamic> datas = result["data"];
  if (datas != null) {
    for (int i = 0; i < datas.length; i++) {
      purchaseList.add(Purchase.fromJson(datas[i]));
    }
  }

  return purchaseList;
}

void addPurchase(
    Function popFunction, totalPrice, productId, sellAmount) async {
  final Map<String, String> jsonBody = {
    "store_id": storeId,
    "total_price": "$totalPrice",
    "product_id": "$productId",
    "product_amount": "$sellAmount",
    "options": "purchase"
  };

  var response = await http.post(
    "http://localhost:8000/api/transaction",
    headers: {
      HttpHeaders.contentTypeHeader: "application/x",
      "X-Requested-With": "XMLHttpRequest",
      HttpHeaders.authorizationHeader: "Bearer $token",
    },
    body: jsonBody,
  );

  var result = json.decode(response.body);

  if (result["response"] == 1) {
    popFunction();
  }
}
