import 'package:flutter/material.dart';
import 'package:store_management/api/api_auth.dart' as ApiAuth;
import 'package:store_management/pages/home_screen.dart';

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginScreenState();
  }
}

class LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  bool _isHidePassword = true;
  bool _upLoading = false;
  Widget _pwIcon = Icon(Icons.visibility, color: Colors.grey);
  TextEditingController _emailController = new TextEditingController();
  TextEditingController _pwController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      resizeToAvoidBottomInset: false,
      body: Form(
        key: _formKey,
        child: Container(
          // color: Color(0x2261c8ff),
          color: Colors.blue[50],
          child: Column(
            children: <Widget>[
              Expanded(
                flex: 2,
                child: Container(
                    width: double.infinity,
                    padding: EdgeInsets.only(bottom: 50, left: 20),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "Store",
                          style: TextStyle(
                              fontSize: 40,
                              fontWeight: FontWeight.bold,
                              color: Color(0xFF2949ba)),
                        ),
                        Text(
                          "Management",
                          style: TextStyle(
                              fontSize: 30,
                              fontWeight: FontWeight.bold,
                              color: Color(0xFF1f3582)),
                        ),
                        Text(
                          "System",
                          style: TextStyle(
                              fontSize: 23,
                              fontWeight: FontWeight.bold,
                              color: Color(0xFF2949ba)),
                        ),
                      ],
                    )),
              ),
              Expanded(
                flex: 3,
                child: Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(10),
                        child: Text(
                          "Email",
                          style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 10, right: 10),
                        child: TextFormField(
                          keyboardType: TextInputType.emailAddress,
                          controller: _emailController,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please enter the email';
                            } else if (!value.contains("@")) {
                              return 'Please enter a valid email';
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            hintText: "email@email.com...",
                            isDense: true,
                            border: OutlineInputBorder(),
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(10),
                        child: Text(
                          "Password",
                          style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 10, right: 10),
                        child: TextFormField(
                          controller: _pwController,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please enter the password';
                            } else if (value.length < 6) {
                              return 'Password length should be more than 6';
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            hintText: "Type here...",
                            isDense: true,
                            border: OutlineInputBorder(),
                            suffixIcon: InkWell(
                              child: _pwIcon,
                              onTap: () {
                                setState(() {
                                  _isHidePassword = !_isHidePassword;
                                  _pwIcon = _isHidePassword
                                      ? Icon(
                                          Icons.visibility,
                                          color: Colors.grey,
                                        )
                                      : Icon(
                                          Icons.visibility_off,
                                          color: Colors.blue,
                                        );
                                });
                              },
                            ),
                          ),
                          obscureText: _isHidePassword,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            left: 50, top: 50, right: 50, bottom: 10),
                        child: Container(
                          width: double.infinity,
                          child: RaisedButton(
                            padding: EdgeInsets.all(13),
                            color: Colors.lightBlue[800],
                            child: _upLoading
                                ? LinearProgressIndicator()
                                : Text(
                                    "Login",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 18,
                                    ),
                                  ),
                            onPressed: () {
                              if (!_upLoading) {
                                _submitBtn();
                              }
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _submitBtn() {
    if (_formKey.currentState.validate()) {
      setState(() {
        _upLoading = true;
      });

      ApiAuth.Login(() {
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (context) => HomeScreen()),
        );
      }, _emailController.text.trim(), _pwController.text.trim());
    }
  }
}
