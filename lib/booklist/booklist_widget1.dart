import 'package:flutter/material.dart';
import 'package:store_management/booklist/booklist_widget2.dart';

class BooklistWidget1 extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _BooklistState();
  }
}

class _BooklistState extends State<BooklistWidget1> {
  bool isWillGet = true;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    final topbutton =  Row(children: <Widget>[
      
      _buildSubItem1('Will Pay', Colors.red),
      _buildSubItem1('Will Get', Colors.blue),
    ])
    ;

    final listview = buildlist(context);
    return Column(children: <Widget>[topbutton, listview]);
  }

  Widget buildlist(BuildContext context) {
    if (isWillGet) {
      return
          listviewwidget(context);
          // Expanded(child: _myListView(context))
          // Text("WIll Get");
    } else {
      return Text("Will Pay");
    }
  }

  Widget listviewwidget(BuildContext context) {
    return Expanded(
      //  flex: 92,
      child:
    // return 
    Container(
      margin: EdgeInsets.all(8),
      decoration: BoxDecoration(
          border: Border.all(color: Colors.blue),
          borderRadius: BorderRadius.all(Radius.circular(8))),
      child: Padding(
        padding: EdgeInsets.all(8),
        child:
            //  Text('data'),
            _myListView(context),
      ),
      ),
    );
  }

  Widget _myListView(BuildContext context) {
    final titles = [
      'bike',
      'boat',
      'bus',
      'car',
      'railway',
      'run',
      'subway',
      'transit',
      'walk'
    ];

    final icons = [
      Icons.directions_bike,
      Icons.directions_boat,
      Icons.directions_bus,
      Icons.directions_car,
      Icons.directions_railway,
      Icons.directions_run,
      Icons.directions_subway,
      Icons.directions_transit,
      Icons.directions_walk
    ];

    return ListView.builder(
      itemCount: titles.length,
      itemBuilder: (context, index) {
        return Container(
          height: 30,
          child:Card(
          //                           <-- Card widget

          child: ListTile(
            onTap: () {
              print('tapped $index');
            },
            leading: Icon(icons[index]),
            title: Text(titles[index]),
          ),
        ),
        );
      },
        );
        
    // );
  }

  Widget _buildSubItem2(String topValue, String bottomValue) {
    return Flexible(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(topValue),
          Text(bottomValue),
        ],
      ),
    );
  }

  Widget _buildSubItem1(String topValue, Color bgColor) {
    return Expanded(
      child: Container(
        margin: EdgeInsets.all(5),
        decoration: BoxDecoration(
            border: Border.all(color: Colors.blue),
            borderRadius: BorderRadius.all(Radius.circular(8))),
        child: RaisedButton(
          color: bgColor,
          shape: new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(8.0),
          ),
          onPressed: () {
            print(topValue);
            isWillGet = (topValue == 'Will Get');
            setState(() {});
            // if(topValue == 'Will Get'){
            //   isWillGet = true;

            // }else{
            //   isWillGet = false;
            // }
          },
          child: Text(
            topValue,
            style: TextStyle(color: Colors.white, fontSize: 17.0),
          ),
        ),
      ),
    );
  }

// class BooklistWidget1 extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     // final date = Text('Today, May 30 2019');

//     return Expanded(
//       flex: 8,
//       child: Container(
//         padding: EdgeInsets.all(5),

//         // margin: EdgeInsets.all(3),
//         // decoration: BoxDecoration(
//         //     border: Border.all(color: Colors.blue),
//         //     borderRadius: BorderRadius.all(Radius.circular(8))),
//         child: Column(
//           children: <Widget>[
//             // date,
//             Flexible(
//               child: Row(
//                 children: <Widget>[
//                   _buildSubItem1('Will Pay', Colors.red),
//                   _buildSubItem1('Will Get', Colors.blue),
//                 ],
//               ),
//             ),
//           ],
//         ),
//       ),
//     );
//   }

// }
}
