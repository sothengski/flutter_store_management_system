import 'package:flutter/material.dart';

class BooklistWidget2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    final verticleLine = Container(
      width: 2,
      color: Colors.blue,
    );

    return Flexible(
      flex: 92,
      child: Container(
        margin: EdgeInsets.all(8),
        decoration: BoxDecoration(
            border: Border.all(color: Colors.blue),
            borderRadius: BorderRadius.all(Radius.circular(8))),
        child: Padding(
          padding: EdgeInsets.all(8),
          child:
              _myListView(context),
              // _buildSubItem2('4', 'Products'),
              // verticleLine,
              // _buildSubItem2('1', 'Out of stock')
            
          
        ),
      ),
    );
  }

  Widget _buildSubItem2(String topValue, String bottomValue) {
    return Flexible(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(topValue),
          Text(bottomValue),
        ],
      ),
    );
  }
 Widget _myListView(BuildContext context) {
      // return ListView.separated(
      //   itemCount: 1000,
      //   itemBuilder: (context, index) {
      //     return ListTile(
      //       title: Text('row $index'),
      //     );
      //   },
      //   separatorBuilder: (context, index) {
      //     return Divider();
      //   },
      // );
       final titles = ['bike', 'boat', 'bus', 'car',
      'railway', 'run', 'subway', 'transit', 'walk'];

      final icons = [Icons.directions_bike, Icons.directions_boat,
      Icons.directions_bus, Icons.directions_car, Icons.directions_railway,
      Icons.directions_run, Icons.directions_subway, Icons.directions_transit,
      Icons.directions_walk];

      return ListView.builder(
        itemCount: titles.length,
        itemBuilder: (context, index) {
          return Card( //                           <-- Card widget
            
            child: ListTile(
              onTap: () {
                print('tapped $index');
                },
              leading: Icon(icons[index]),
              title: Text(titles[index]),
              
            ),
          );
        },
      );
}

}