import 'package:flutter/material.dart';

class StudentList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _State();
  }
}

class _State extends State<StudentList> {
  List maleStudents = ["Mr. A", "Mr. B", "Mr. C"];
  List femaleStudents = ["Ms. X", "Ms. Y", "Ms. Z"];

  var isMaleSelected = true;

  @override
  Widget build(BuildContext context) {
    final maleTabBg = isMaleSelected ? Colors.blue : Colors.white;
    final femaleTabBg = isMaleSelected ? Colors.white : Colors.blue;

    Widget tabWidget = Row(
      children: <Widget>[
        Expanded(
          child: GestureDetector(
            onTap: () {
              isMaleSelected = true;
              setState((){});
            },
            child: Container(
              height: 50,
              color: maleTabBg,
              child: Text('Male'),
            ),
          ),
        ),
        Expanded(
          child: GestureDetector(
            onTap: () {
              isMaleSelected = false;
              setState((){});
            },
            child: Container(
              height: 50,
              color: femaleTabBg,
              child: Text('Female'),
            ),
          ),
        ),
      ],
    );
    final data = isMaleSelected ? maleStudents : femaleStudents;
    final listWidget = ListView.builder(
      itemCount: data.length,
      itemBuilder: (context, index){
        final student = data[index];
        return ListTile(title: Text(student),);
      },
    );

    final body = Column(
      children: <Widget>[
        tabWidget,
        Expanded(
          child: listWidget,
        )
      ],
    );

    return Expanded(
      child: Container(
      // appBar: AppBar(
      //   title: Text('Students List'),
      // ),
      child: Column(
        children: <Widget>[
          tabWidget,
        Expanded(
          child: listWidget,
        )
        ]
      ),
    )
    );
  }
}
