import 'package:flutter/material.dart';

class StudentList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _State();
  }
}

class _State extends State<StudentList> {
  List<Student> students = [
                            Student(1, 'Mr. A', 'M'),
                            Student(2, 'Ms. B', 'F'),
                            Student(3, 'Mr. C', 'M'),
                            Student(4, 'Ms. D', 'F'),
                            Student(5, 'Mr. E', 'M'),
                          ];

  var isMaleSelected = true;

  @override
  Widget build(BuildContext context) {
    final maleTabBg = isMaleSelected ? Colors.blue : Colors.white;
    final femaleTabBg = isMaleSelected ? Colors.white : Colors.blue;

    Widget tabWidget = Row(
      children: <Widget>[
        Expanded(
          child: GestureDetector(
            onTap: () {
              isMaleSelected = true;
              setState((){});
            },
            child: Container(
              height: 50,
              color: maleTabBg,
              child: Text('Male'),
            ),
          ),
        ),
        Expanded(
          child: GestureDetector(
            onTap: () {
              isMaleSelected = false;
              setState((){});
            },
            child: Container(
              height: 50,
              color: femaleTabBg,
              child: Text('Female'),
            ),
          ),
        ),
      ],
    );
    final data = students.where((student){
      final gender = isMaleSelected ? 'M':'F';
      return student.gender==gender;

    }).toList();
    final listWidget = ListView.builder(
      itemCount: data.length,
      itemBuilder: (context, index){
        final student = data[index];
        return ListTile(title: Text(student.fullName),);
      },
    );

    final body = Column(
      children: <Widget>[
        tabWidget,
        Expanded(
          child: listWidget,
        )
      ],
    );
    return Expanded(
      child: Container(
        child: Column(
      children: <Widget>[
        tabWidget,
        Expanded(
          child: listWidget,
        )
      ],
    ),
      ),
    );
  //   return Scaffold(
  //     appBar: AppBar(
  //       title: Text('Students List'),
  //     ),
  //     body: body,
  //   );
  }
}

class Student {
  int id;
  String fullName;
  String gender;

  Student(this.id, this.fullName, this.gender);
}