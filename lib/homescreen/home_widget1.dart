import 'package:flutter/material.dart';

class HomeWidget1 extends StatelessWidget {
  final verticleLine = Container(
    height: 5,
  );
  @override
  Widget build(BuildContext context) {
    final date = Padding(
      padding: EdgeInsets.only(top: 5),
      child: Text(
        'Today, June 30 2019',
        style: TextStyle(fontSize: 23),
      ),
    );

    return Expanded(
      flex: 25,
      child: Container(
        padding: EdgeInsets.only(left: 5, right: 5),
        margin: EdgeInsets.fromLTRB(8, 2, 8, 2),
        decoration: BoxDecoration(
            border: Border.all(color: Colors.blue),
            borderRadius: BorderRadius.all(Radius.circular(8))),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            date,
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                _buildSubItem1('Expense', '\$10.00', Colors.red),
                _buildSubItem1('Income', '\$200.00', Colors.blue),
              ],
            ),
            verticleLine,
            verticleLine,
            Padding(
              padding: EdgeInsets.only(bottom: 5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Profit Margin: ',
                    style: TextStyle(fontSize: 20.0),
                  ),
                  Text(
                    '\$190.00',
                    style: TextStyle(color: Colors.blue, fontSize: 20.0),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildSubItem1(String topValue, String bottomValue, Color bgColor) {
    return Expanded(
      // fit: FlexFit.loose,
      child: Container(
          height: 110,
          margin: EdgeInsets.all(5),
          decoration: BoxDecoration(
              border: Border.all(color: Colors.blue),
              color: bgColor,
              borderRadius: BorderRadius.all(Radius.circular(8))),
          child: Column(
            // crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              verticleLine,
              verticleLine,
              Text(
                topValue,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 25,
                  fontWeight: FontWeight.w100,
                ),
              ),
              verticleLine,
              verticleLine,
              verticleLine,
              Text(
                bottomValue,
                style: TextStyle(color: Colors.white, fontSize: 35),
              ),
            ],
          )),
    );
  }
}
