import 'package:flutter/material.dart';

class HomeWidget2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final verticleLine = Container(
      width: 2,
      color: Colors.blue,
    );

    return Expanded(
      flex: 10,
      // fit: FlexFit.loose,
      child: Container(
        margin: EdgeInsets.all(8),
        decoration: BoxDecoration(
            border: Border.all(color: Colors.blue),
            borderRadius: BorderRadius.all(Radius.circular(8))),
        child: Padding(
          padding: EdgeInsets.all(5),
          child: Row(
            children: <Widget>[
              _buildSubItem2('4', 'Products'),
              verticleLine,
              _buildSubItem2('1', 'Out of stock')
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildSubItem2(String topValue, String bottomValue) {
    return Expanded(
      // fit: FlexFit.loose,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            topValue,
            style: TextStyle(color: Colors.red, fontSize: 25),
          ),
          Text(
            bottomValue,
            style: TextStyle(color: Colors.black, fontSize: 16),
          ),
        ],
      ),
    );
  }
}
