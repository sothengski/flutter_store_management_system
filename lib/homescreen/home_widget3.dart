import 'package:flutter/material.dart';
import 'package:store_management/product_list/product_list.dart';
import 'package:store_management/profit_loss/profit_loss.dart';
import 'package:store_management/purchase/purchase_list.dart';
import 'package:store_management/sale/sale_list.dart';

class HomeWidget3 extends StatelessWidget {
  final verticleLine = Container(
    height: 15,
  );
  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 45,
      child: Container(
        padding: EdgeInsets.fromLTRB(8.0, 15.0, 8.0, 5.0),
        margin: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 15.0),
        decoration: BoxDecoration(
            border: Border.all(color: Colors.lightBlue),
            borderRadius: BorderRadius.all(Radius.circular(8))),
        child: SingleChildScrollView(
          child: Row(
            children: <Widget>[
              Expanded(
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          _buildSubitem(
                            Icons.add_shopping_cart,
                            'Purchase List',
                            Colors.pink[400],
                            () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => PurchaseListScreen()),
                              );
                            },
                          ),
                          _buildSubitem(
                            Icons.storage,
                            'Product List',
                            Colors.deepOrangeAccent,
                            () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ProductListScreen()),
                              );
                            },
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      // fit: FlexFit.loose,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          _buildSubitem(
                            Icons.remove_shopping_cart,
                            'Sales List',
                            Colors.blue[400],
                            () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => SaleListScreen()),
                              );
                            },
                          ),
                          _buildSubitem(
                            Icons.trending_up,
                            'Profit Loss',
                            Colors.greenAccent[400],
                            () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ProfitLossScreen()),
                              );
                            },
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildSubitem(
      IconData icondata, String title, Color bgColor, Function action) {
    return Container(
      margin: EdgeInsets.all(8),
      decoration: BoxDecoration(
        border: Border.all(color: Colors.blue),
        borderRadius: BorderRadius.all(Radius.circular(8)),
        // color: bgColor,
      ),
      child: RaisedButton(
        color: bgColor,
        shape: new RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(8.0),
        ),
        onPressed: action,
        child: Column(
          children: <Widget>[
            verticleLine,
            verticleLine,
            Icon(
              icondata,
              size: 60,
              color: Colors.white,
            ),
            verticleLine,
            Text(
              title,
              style: TextStyle(color: Colors.white, fontSize: 16),
            ),
            verticleLine,
          ],
        ),
      ),
    );
  }
}
