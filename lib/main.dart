import 'package:flutter/material.dart';
// import 'package:store_management/pages/home_screen.dart';
import 'package:store_management/auth/login.dart';

void main() {
  final myApp = MyApp();
  runApp(myApp);
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // final registerscreen = RegisterScreen();
    // final homeScreen = HomeScreen();
    final loginScreen = LoginScreen();

    final materialApp = MaterialApp(
      title: 'Store Management',
      home: loginScreen,
      // home: PurchaseList(),
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
    );

    return materialApp;
  }
}
