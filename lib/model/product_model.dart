class Product {
  var id, name, categoryId, amount, priceIn, priceOut, unit, image, createAt;

  Product(
    this.id,
    this.name,
    this.categoryId,
    this.amount,
    this.priceIn,
    this.priceOut,
    this.unit,
    this.image,
    this.createAt,
  );

  Product.fromJson(jsonData) {
    this.id = jsonData["id"];
    this.name = jsonData["name"];
    this.categoryId = jsonData["category_id"];
    this.amount = jsonData["amount"];
    this.priceIn = jsonData["price_in"];
    this.priceOut = jsonData["price_out"];
    this.unit = jsonData["measure_unit"];
    this.image = jsonData["image_path"];
    this.createAt = jsonData["created_at"];
  }
}
