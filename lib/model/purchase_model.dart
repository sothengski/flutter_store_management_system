class Purchase {
  var imgUrl, title, quantity, purchasePrice, totalPrice, date, measureUnit;

  Purchase(this.imgUrl, this.title, this.quantity, this.purchasePrice,
      this.totalPrice, this.date, this.measureUnit);

  Purchase.fromJson(jsonData) {
    var transactionInfo = jsonData["transaction_info"][0];
    this.imgUrl = transactionInfo["image_path"];
    this.title = transactionInfo["name"];
    this.quantity = transactionInfo["amount"];
    this.purchasePrice = transactionInfo["price_in"];
    this.totalPrice = jsonData["total_price"];
    this.date = jsonData["created_at"];
    this.measureUnit = transactionInfo["measure_unit"];
  }
}
