class Sale {
  var imgUrl, title, quantity, sellPrice, totalPrice, date, measureUnit;

  Sale(this.imgUrl, this.title, this.quantity, this.sellPrice, this.totalPrice,
      this.date, this.measureUnit);

  Sale.fromJson(jsonData) {
    var transactionInfo = jsonData["transaction_info"][0];
    this.imgUrl = transactionInfo["image_path"];
    this.title = transactionInfo["name"];
    this.quantity = transactionInfo["amount"];
    this.sellPrice = transactionInfo["price_out"];
    this.totalPrice = jsonData["total_price"];
    this.date = jsonData["created_at"];
    this.measureUnit = transactionInfo["measure_unit"];
  }
}
