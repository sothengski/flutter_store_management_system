import 'package:flutter/material.dart';

class BookEntryList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _State();
  }
}

class _State extends State<BookEntryList> {
  List<Client> clients = [
    Client(1, 'Mr. A', '070434448', '50', 'P'),
    Client(2, 'Ms. B', '070434448', '150', 'G'),
    Client(3, 'Mr. C', '070434448', '250', 'G'),
    Client(4, 'Ms. D', '070434448', '350', 'P'),
    Client(5, 'Mr. E', '070434448', '450', 'G'),
    Client(6, 'Mr. F', '070434448', '430', 'P'),
    Client(7, 'Mr. G', '070434448', '10', 'G'),
    Client(8, 'Mr. H', '070434448', '120', 'P'),
    Client(9, 'Mr. A', '070434448', '50', 'P'),
    Client(10, 'Ms. B', '070434448', '150', 'P'),
    Client(11, 'Mr. C', '070434448', '250', 'P'),
    Client(12, 'Ms. D', '070434448', '350', 'P'),
    Client(13, 'Mr. E', '070434448', '450', 'P'),
    Client(14, 'Mr. F', '070434448', '430', 'P'),
    Client(15, 'Mr. G', '070434448', '10', 'G'),
    Client(16, 'Mr. H', '070434448', '120', 'P'),
  ];
  List<String> images = [
    'assets/img/1.jpg',
    'assets/img/b1.jpg',
    'assets/img/3.jpg',
    'assets/img/2.jpg',
    'assets/img/4.jpg',
    'assets/img/5.jpg',
    'assets/img/b2.jpg',
    'assets/img/6.jpg'
  ];

  var isGetSelected = true;

  @override
  Widget build(BuildContext context) {
    final getTabBg = isGetSelected ? Colors.blue : Colors.white;
    final payTabBg = isGetSelected ? Colors.white : Colors.red;

    Widget tabWidget = Row(
      children: <Widget>[
        Expanded(
          child: GestureDetector(
            onTap: () {
              isGetSelected = true;
              setState(() {});
            },
            child: Container(
                height: 50,
                color: getTabBg,
                child: Center(
                    child: Text(
                  'Will Get',
                  style: TextStyle(
                      fontSize: 18.0,
                      fontWeight: FontWeight.bold,
                      color: Colors.black),
                ))),
          ),
        ),
        Expanded(
          child: GestureDetector(
            onTap: () {
              isGetSelected = false;
              setState(() {});
            },
            child: Container(
                height: 50,
                color: payTabBg,
                child: Center(
                  child: Text(
                    'Will Pay',
                    style: TextStyle(
                        fontSize: 18.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.black),
                  ),
                )),
          ),
        ),
        
      ],
    );
    final floatingActionButton= FloatingActionButton(
        onPressed: () {
        // Add your onPressed code here!
        });
    final data = clients.where((client) {
      final typeoftransaction = isGetSelected ? 'G' : 'P';
      return client.typeoftransaction == typeoftransaction;
    }).toList();
    final listWidget = ListView.builder(
      itemCount: data.length,
      itemBuilder: (context, index) {
        final client = data[index];
        return Container(
            height: 100,
            child: Card(
                child: Center(
                    child: ListTile(
              leading: CircleAvatar(
                radius: 40,
                backgroundImage: AssetImage(images[index % images.length]),
              ),
              title: Text.rich(
                TextSpan(
                  children: <TextSpan>[
                  TextSpan(text: 'Name:      '),
                  TextSpan(text: client.fullName +'\n',style: TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                    )),
                  
                  TextSpan(text: 'Contact:  '),
                  TextSpan(text: client.phonenumber,style: TextStyle(
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold,
                    )),
                  ],),),
              subtitle: Text(
                'Amount: \$ ' + client.cost,
                style: TextStyle(
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold,
                    color: isGetSelected ? Colors.blue : Colors.red),
              ),
            ))));
      },
    );

    

    final body = Column(
      children: <Widget>[
        tabWidget,
        Expanded(
          child: listWidget,
        )
      ],
    );
    return Expanded(
      child: Container(
        child: Column(
          children: <Widget>[
            tabWidget,

            Expanded(
              child: listWidget,
            )
          ],
        ),
      ),
    );
    //   return Scaffold(
    //     appBar: AppBar(
    //       title: Text('Students List'),
    //     ),
    //     body: body,
    //   );
  }
}

class Client {
  int id;
  String fullName;
  String phonenumber;
  String cost;
  String typeoftransaction;

  Client(this.id, this.fullName, this.phonenumber, this.cost,
      this.typeoftransaction);
}
