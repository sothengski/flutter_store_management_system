import 'package:flutter/material.dart';
// import 'package:store_management/booklist/student_list2.dart';
import 'package:store_management/homescreen/home_widget1.dart';
import 'package:store_management/homescreen/home_widget2.dart';
import 'package:store_management/homescreen/home_widget3.dart';

import 'package:fancy_bottom_navigation/fancy_bottom_navigation.dart';
import 'package:store_management/pages/book_list.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int currentPage = 0;

  GlobalKey bottomNavigationKey = GlobalKey();
  @override
  Widget build(BuildContext context) {
    final appBar = AppBar(
      backgroundColor: Colors.blue,
      title: Text('My Store'),
      actions: <Widget>[
        IconButton(
          icon: Icon(Icons.notifications),
          onPressed: () {
            print('Click on notification.');
          },
        )
      ],
    );

    final appBarbooklist = AppBar(
      backgroundColor: Colors.red,
      title: Text('Booklist'),
      actions: <Widget>[
        IconButton(
          icon: Icon(Icons.add),
          onPressed: () {
            print('Click on notification in Booklist.');
          },
        )
      ],
    );
    // final body = Container(
    //   decoration: BoxDecoration(color: Colors.white),
    //   child: Center(
    //     child: _getPage(currentPage),
    //   ),
    // );
    final homebody = Column(
      // mainAxisSize: MainAxisSize.min,
      // crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        HomeWidget1(),
        HomeWidget2(),
        HomeWidget3(),
      ],
    );
    final booklistbody = Column(children: <Widget>[
      BookEntryList()
      // BooklistWidget1(),
      // BooklistWidget2(),
    ]);

    final bottomNavigationBar = FancyBottomNavigation(
      // circleColor: Colors.blue,
      // inactiveIconColor: Colors.blue,
      tabs: [
        TabData(
            iconData: Icons.home,
            title: "Home",
            onclick: () {
              final FancyBottomNavigationState fState =
                  bottomNavigationKey.currentState;
              fState.setPage(0);
            }),
        TabData(
          iconData: Icons.shopping_cart,
          title: "Sale",
          // onclick: () => Navigator.of(context)
          // .push(MaterialPageRoute(builder: (context) => SecondPage()))
        ),
        TabData(
            iconData: Icons.monetization_on,
            title: "BookList",
            onclick: () {
              final FancyBottomNavigationState fState =
                  bottomNavigationKey.currentState;
              fState.setPage(2);
            }
            // onclick: () => Navigator.of(context)
            //     .push(MaterialPageRoute(builder: (context) => booklistbody))
            ),
        TabData(
          iconData: Icons.settings,
          title: "Setting",
          // onclick: () => Navigator.of(context)
          //     .push(MaterialPageRoute(builder: (context) => SecondPage()))
        ),
      ],

      // print('Click on tab $tabIndex');
      // if (tabIndex == 2) {
      //   Navigator.push(
      //     context,
      //     MaterialPageRoute(builder: (context) => Booklist()),
      //   );
      // },

      initialSelection: 0,
      key: bottomNavigationKey,
      onTabChangedListener: (position) {
        // onTabChangedListener: (position) {
        //   if (position == 2) {
        //     Navigator.of(context)
        //         .push(MaterialPageRoute(builder: (context) => Booklist())
        //         );
        //   }
        print(position);
        setState(() {
          currentPage = position;
        });
      },
    );

    _getPage(int page) {
      switch (page) {
        case 0:
          return Container(
            child: homebody,
          );
        // case 1:
        //   return Column();
        case 2:
          return Container(
            child: booklistbody,
          );
      }
    }

    return Scaffold(
      resizeToAvoidBottomPadding: false,
      resizeToAvoidBottomInset: false,
      appBar: (currentPage == 2) ? appBarbooklist : appBar,
      bottomNavigationBar: bottomNavigationBar,
      body:
          // homebody,
          Container(
        // decoration: BoxDecoration(color: Colors.white),
        // height: 300.0,
        child: _getPage(currentPage),
      ),
    );
  }
}
