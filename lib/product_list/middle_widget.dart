import 'package:flutter/material.dart';

class MiddleWidget extends StatelessWidget {
  Widget build(BuildContext context) {
    return Expanded(
        flex: 1,
        child: Container(
          padding: EdgeInsets.all(8),
          margin: EdgeInsets.all(15),
          decoration: BoxDecoration(
            border: Border.all(color: Colors.blue),
            borderRadius: BorderRadius.all(Radius.circular(5)),
            color: Colors.white,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Icon(Icons.search),
            ],
          ),
        ));
  }
}
