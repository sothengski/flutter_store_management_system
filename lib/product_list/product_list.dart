import 'package:flutter/material.dart';
import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';

import 'package:store_management/model/product_model.dart';
import 'package:store_management/api/api_load_product.dart' as ApiProductList;
import 'package:store_management/product_list/add_product.dart';
import 'package:store_management/data.dart' as Data;
import 'upper_widget.dart';
import 'middle_widget.dart';

class ProductListScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ProductListState();
  }
}

class ProductListState extends State<ProductListScreen> {
  List<Product> productData = [];
  ScrollController _scrollController = new ScrollController();

  @override
  void initState() {
    super.initState();
    ApiProductList.getAllProduct().then((data) {
      setState(() {
        productData = data;
      });
    });
  }

  void refreshData() {
    setState(() {
      ApiProductList.getAllProduct().then((data) {
        setState(() {
          productData = data;
        });
      });
      _scrollController.animateTo(
        _scrollController.position.maxScrollExtent,
        duration: const Duration(milliseconds: 500),
        curve: Curves.easeInOut,
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        backgroundColor: Colors.red[300],
        centerTitle: true,
        title: Text(
          "Product List",
          style: TextStyle(
            color: Colors.white,
            fontSize: 25,
            fontWeight: FontWeight.bold,
          ),
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.search,
              size: 30,
            ),
            onPressed: () {},
          )
        ],
      ),
      body: _buildBody(),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showDialog(
            context: context,
            builder: (context) => AddProduct(refreshData),
          );
        },
        child: Icon(Icons.add),
      ),
    );
  }

  Widget _buildSubItem2(String topValue, String bottomValue) {
    return Expanded(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            topValue,
            style: TextStyle(fontSize: 16),
          ),
          Text(
            bottomValue,
            style: TextStyle(fontSize: 16),
          ),
        ],
      ),
    );
  }

  Widget _buildBody() {
    final verticleLine = Container(
      width: 2,
      color: Colors.blue,
    );
    return Column(
      children: <Widget>[
        Expanded(
          flex: 1,
          child: Container(
            margin: EdgeInsets.all(8),
            decoration: BoxDecoration(
                border: Border.all(color: Colors.blue),
                borderRadius: BorderRadius.all(Radius.circular(8))),
            child: Padding(
              padding: EdgeInsets.all(5),
              child: Row(
                children: <Widget>[
                  _buildSubItem2('4', 'Products'),
                  verticleLine,
                  _buildSubItem2('1', 'Out of stock')
                ],
              ),
            ),
          ),
        ),
        Expanded(
          flex: 1,
          child: Container(
            padding: EdgeInsets.all(8),
            margin: EdgeInsets.all(15),
            decoration: BoxDecoration(
              border: Border.all(color: Colors.blue),
              borderRadius: BorderRadius.all(Radius.circular(5)),
              color: Colors.white,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Icon(Icons.search),
              ],
            ),
          ),
        ),
        Expanded(
          flex: 8,
          child: Container(
            color: Colors.white24,
            child: productData.length > 0
                ? ListView.builder(
                    controller: _scrollController,
                    itemCount: productData.length,
                    itemBuilder: (context, index) {
                      var element = productData[index];
                      return _buildItemCard(
                        element.image,
                        element.name,
                        element.amount,
                        element.priceOut,
                        element.createAt,
                        element.unit,
                      );
                    },
                  )
                : Container(
                    width: 50,
                    height: 50,
                    alignment: Alignment.center,
                    child: CircularProgressIndicator(),
                  ),
          ),
        ),
      ],
    );
  }

  Widget _buildItemCard(image, name, amount, priceOut, createAt, unit) {
    var token = Data.token;
    return Container(
      // padding: EdgeInsets.all(10),
      padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
      margin: EdgeInsets.all(10),
      decoration: BoxDecoration(
        boxShadow: <BoxShadow>[
          BoxShadow(
            offset: Offset(0, 3),
            color: Colors.black12,
            blurRadius: 6,
          )
        ],
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      // color: Colors.white,
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Padding(
              padding: EdgeInsets.only(right: 5),
              child: CachedNetworkImage(
                imageUrl: "http://localhost:8000/api/product/image/$image",
                httpHeaders: {
                  HttpHeaders.contentTypeHeader:
                      "application/x-www-form-urlencoded",
                  "X-Requested-With": "XMLHttpRequest",
                  HttpHeaders.authorizationHeader: "Bearer $token",
                },
                placeholder: (context, url) => new CircularProgressIndicator(),
                errorWidget: (context, url, error) => new Icon(Icons.error),
                width: 100,
                height: 100,
              ),
            ),
          ),
          Expanded(
            flex: 3,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  height: 90,
                  width: 1,
                  color: Colors.black,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      child: Text(
                        name,
                        style: TextStyle(
                            color: Colors.blue[900],
                            fontSize: 22,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 5, bottom: 5),
                      child: Row(
                        children: <Widget>[
                          Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "Quantity:",
                                  style: TextStyle(
                                    color: Colors.blue[900],
                                  ),
                                ),
                                Text(
                                  "Purchasing Price:",
                                  style: TextStyle(
                                    color: Colors.blue[900],
                                  ),
                                ),
                                Text(
                                  "Selling Price:",
                                  style: TextStyle(
                                    color: Colors.blue[900],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            width: 30,
                          ),
                          Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "$amount $unit",
                                  style: TextStyle(color: Colors.orange[800]),
                                ),
                                Text(
                                  "\$ $priceOut",
                                  style: TextStyle(color: Colors.orange[800]),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      child: Row(
                        children: <Widget>[
                          Text(
                            "Date:",
                            style: TextStyle(
                              color: Colors.blue[900],
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            createAt,
                            style: TextStyle(color: Colors.orange[800]),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Container(
                  height: 90,
                  width: 1,
                  color: Colors.black,
                ),
              ],
            ),
          ),
          Expanded(
              flex: 1,
              child: Container(
                child: Icon(Icons.edit),
                // Text(
                //   textAlign: TextAlign.center,
                //   style: TextStyle(color: Colors.blue[700]),
                // ),
              )),
        ],
      ),
    );
  }
}
