import 'package:flutter/material.dart';

class UpperWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final verticleLine = Container(
      width: 2,
      color: Colors.blue,
    );

    return Expanded(
      flex: 1,
      child: Container(
        margin: EdgeInsets.all(8),
        decoration: BoxDecoration(
          border: Border.all(color: Colors.blue),
          borderRadius: BorderRadius.all(Radius.circular(8)),
          color: Colors.white,
        ),
        child: Padding(
          padding: EdgeInsets.all(8),
          child: Row(
            children: <Widget>[
              _buildSubItem2('4', 'Products'),
              verticleLine,
              _buildSubItem2('1', 'Out of stock')
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildSubItem2(String topValue, String bottomValue) {
    return Expanded(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            topValue,
            style: TextStyle(fontSize: 16),
          ),
          Text(
            bottomValue,
            style: TextStyle(fontSize: 16),
          ),
        ],
      ),
    );
  }
}
