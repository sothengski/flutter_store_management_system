import 'package:flutter/material.dart';

class ProfitLossScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        backgroundColor: Colors.greenAccent[400],
        centerTitle: true,
        title: Text(
          "Profit Loss",
          style: TextStyle(
            color: Colors.white,
            fontSize: 25,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: _buildBody(),
    );
  }

  Widget _buildBody() {
    return Column(
      children: <Widget>[
        Expanded(
          flex: 2,
          child: Container(
            color: Colors.white24,
            child: _buildDatePick(),
          ),
        ),
        Expanded(
          flex: 8,
          child: Container(
            color: Colors.white24,
            child: Column(
              children: <Widget>[
                _buildCardReview("Income Total", "\$30.00"),
                _buildCardReview("Expense Total", "\$15.00"),
                _buildCardReview("Profit Margin", "\$15.00"),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildDatePick() {
    return Column(
      children: <Widget>[
        Expanded(
          flex: 2,
          child: Container(
            padding: EdgeInsets.all(10),
            color: Colors.blue[50],
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "Today",
                      style: TextStyle(fontSize: 12),
                    ),
                    Text(
                      "March 2019",
                      style: TextStyle(fontSize: 12),
                    ),
                    Text(
                      "Today",
                      style: TextStyle(color: Colors.transparent),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    _buildDateCircle("M", "4", Colors.grey[300]),
                    _buildDateCircle("T", "5", Colors.grey[300]),
                    _buildDateCircle("W", "6", Colors.grey[300]),
                    _buildDateCircle("Th", "7", Colors.grey[300]),
                    _buildDateCircle("F", "8", Colors.grey[300]),
                    _buildDateCircle("S", "9", Colors.blue[300]),
                    _buildDateCircle("Su", "10", Colors.grey[300]),
                  ],
                )
              ],
            ),
          ),
        ),
        Expanded(
          flex: 1,
          child: Container(
            padding: EdgeInsets.only(left: 10, right: 10),
            color: Colors.transparent,
          ),
        ),
      ],
    );
  }

  Widget _buildDateCircle(String date, String number, Color color) {
    return Column(
      children: <Widget>[
        Text(date),
        CircleAvatar(
          backgroundColor: color,
          foregroundColor: Colors.black,
          child: Text(
            number,
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildCardReview(String title, String amount) {
    return Container(
      padding: EdgeInsets.fromLTRB(20, 5, 20, 5),
      margin: EdgeInsets.all(10),
      decoration: BoxDecoration(
        boxShadow: <BoxShadow>[
          BoxShadow(
            offset: Offset(0, 3),
            color: Colors.black12,
            blurRadius: 6,
          )
        ],
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(top: 20, bottom: 20),
            child: Text(
              title,
              style: TextStyle(
                fontSize: 20,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 20, bottom: 20),
            child: Text(
              amount,
              style: TextStyle(
                color: Colors.blue,
                fontSize: 40,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
