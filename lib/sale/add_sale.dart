import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:store_management/model/product_model.dart';
import 'package:store_management/api/api_load_product.dart' as ApiLoadProduct;
import 'package:store_management/api/api_sale_list.dart' as ApiSaleList;

class AddSale extends StatefulWidget {
  Function saleListScreenRefreshData;

  AddSale(this.saleListScreenRefreshData);

  @override
  State<StatefulWidget> createState() {
    return AddSaleState();
  }
}

class AddSaleState extends State<AddSale> {
  List<Product> products = [];
  Product selectedProduct;
  String date = "";
  bool _upLoading = false;
  final _formKey = GlobalKey<FormState>();
  TextEditingController amountController = new TextEditingController();
  TextEditingController priceController = new TextEditingController();

  @override
  void initState() {
    super.initState();
    ApiLoadProduct.getAllProduct().then((value) {
      setState(() {
        products = value;
        selectedProduct = products[0];
        amountController.text = "1";
        priceController.text = selectedProduct.priceOut;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(Consts.padding),
      ),
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: dialogContent(context),
    );
  }

  dialogContent(BuildContext context) {
    return Form(
      key: _formKey,
      child: Container(
        padding: EdgeInsets.all(Consts.padding),
        decoration: new BoxDecoration(
          color: Colors.white,
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.circular(Consts.padding),
          boxShadow: [
            BoxShadow(
              color: Colors.black26,
              blurRadius: 10.0,
              offset: const Offset(0.0, 10.0),
            ),
          ],
        ),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  InkWell(
                    child: Icon(
                      Icons.cancel,
                      color: Colors.red[400],
                      size: 30,
                    ),
                    onTap: () {
                      Navigator.pop(context);
                    },
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    "Sell Entry",
                    style: TextStyle(
                      fontSize: 23,
                      fontWeight: FontWeight.bold,
                    ),
                  )
                ],
              ),
              Padding(
                padding: EdgeInsets.all(10),
                child: Text(
                  "PRODUCT ID",
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 10, right: 10),
                child: products.length > 0
                    ? DropdownButton<Product>(
                        isExpanded: true,
                        hint: Text("Choose product id"),
                        value: selectedProduct,
                        icon: Icon(Icons.arrow_drop_down),
                        iconSize: 24,
                        elevation: 16,
                        style: TextStyle(color: Colors.blue[900]),
                        underline: Container(
                          height: 2,
                          color: Colors.blue,
                        ),
                        onChanged: (Product newValue) {
                          setState(() {
                            selectedProduct = newValue;
                            priceController.text = newValue.priceOut;
                          });
                        },
                        items: products
                            .map<DropdownMenuItem<Product>>((Product value) {
                          return DropdownMenuItem<Product>(
                            value: value,
                            child: Text(
                              "${value.id}",
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: 18),
                            ),
                          );
                        }).toList(),
                      )
                    : LinearProgressIndicator(),
              ),
              Padding(
                padding: EdgeInsets.all(10),
                child: Text(
                  "PRODUCT NAME",
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 10, right: 10),
                child: products.length > 0
                    ? DropdownButton<Product>(
                        isExpanded: true,
                        hint: Text("Choose product name"),
                        value: selectedProduct,
                        icon: Icon(Icons.arrow_drop_down),
                        iconSize: 24,
                        elevation: 16,
                        style: TextStyle(color: Colors.blue[900]),
                        underline: Container(
                          height: 2,
                          color: Colors.blue,
                        ),
                        onChanged: (Product newValue) {
                          setState(() {
                            selectedProduct = newValue;
                            priceController.text = newValue.priceOut;
                          });
                        },
                        items: products
                            .map<DropdownMenuItem<Product>>((Product value) {
                          return DropdownMenuItem<Product>(
                            value: value,
                            child: Text(
                              "${value.name}",
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: 18),
                            ),
                          );
                        }).toList(),
                      )
                    : LinearProgressIndicator(),
              ),
              Padding(
                padding: EdgeInsets.all(10),
                child: Text(
                  "QUANTITY",
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 10, right: 10),
                child: TextFormField(
                  keyboardType: TextInputType.number,
                  controller: amountController,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Please enter the amount';
                    } else if (int.parse(value) < 0) {
                      return 'Please enter a valid amount';
                    }
                    return null;
                  },
                  onChanged: (value) {
                    if (int.parse(value) > 0) {
                      double price = int.parse(value) *
                          double.parse(selectedProduct.priceOut);
                      setState(() {
                        priceController.text = "$price";
                      });
                    }
                  },
                  decoration: InputDecoration(
                    hintText: "Type here...",
                    isDense: true,
                    border: OutlineInputBorder(),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(10),
                child: Text(
                  "PRICE",
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 10, right: 10),
                child: TextFormField(
                  keyboardType: TextInputType.number,
                  controller: priceController,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Please enter the price';
                    } else if (double.parse(value) < 0.0) {
                      return 'Please enter a valid price';
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    hintText: "Type here...",
                    suffixIcon: Icon(Icons.attach_money),
                    isDense: true,
                    border: OutlineInputBorder(),
                  ),
                ),
              ),
              Padding(
                padding:
                    EdgeInsets.only(left: 10, top: 15, right: 10, bottom: 10),
                child: Container(
                  width: double.infinity,
                  child: RaisedButton(
                    color: Colors.lightBlue[800],
                    child: _upLoading
                        ? LinearProgressIndicator()
                        : Text(
                            "FINISH",
                            style: TextStyle(color: Colors.white),
                          ),
                    onPressed: () {
                      if (!_upLoading) {
                        _submitBtn();
                      }
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _submitBtn() {
    if (_formKey.currentState.validate()) {
      setState(() {
        _upLoading = true;
      });

      ApiSaleList.addSale(
        () {
          widget.saleListScreenRefreshData();
          Navigator.pop(context);
        },
        priceController.text,
        selectedProduct.id,
        amountController.text,
      );
    }
  }
}

class Consts {
  Consts._();

  static const double padding = 16.0;
  static const double avatarRadius = 66.0;
}
