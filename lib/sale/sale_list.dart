import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:store_management/model/sale_model.dart';
import 'package:store_management/api/api_sale_list.dart' as ApiSaleList;
import 'package:store_management/sale/add_sale.dart';
import 'package:store_management/data.dart' as Data;

class SaleListScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SaleListState();
  }
}

class SaleListState extends State<SaleListScreen> {
  List<Sale> saleData;
  ScrollController _scrollController = new ScrollController();

  @override
  void initState() {
    super.initState();
    ApiSaleList.getAllSaleList().then((data) {
      setState(() {
        saleData = data;
      });
    });
  }

  void refreshData() {
    setState(() {
      ApiSaleList.getAllSaleList().then((data) {
        if (data != null) {
          setState(() {
            saleData = data;
          });
        }
      });
      _scrollController.animateTo(
        _scrollController.position.maxScrollExtent,
        duration: const Duration(milliseconds: 500),
        curve: Curves.easeInOut,
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        backgroundColor: Colors.blue[300],
        centerTitle: true,
        title: Text(
          "Sales List",
          style: TextStyle(
            color: Colors.white,
            fontSize: 25,
            fontWeight: FontWeight.bold,
          ),
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.search,
              size: 30,
            ),
            onPressed: () {},
          )
        ],
      ),
      body: _buildBody(),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showDialog(
            context: context,
            builder: (context) => AddSale(refreshData),
          );
        },
        child: Icon(Icons.add),
      ),
    );
  }

  Widget _buildBody() {
    return Column(
      children: <Widget>[
        Expanded(
          flex: 2,
          child: Container(
            color: Colors.white,
            child: _buildDatePick(),
          ),
        ),
        Expanded(
          flex: 8,
          child: Container(
            color: Colors.white24,
            child: saleData != null
                ? saleData.length > 0
                    ? ListView.builder(
                        controller: _scrollController,
                        itemCount: saleData.length,
                        itemBuilder: (context, index) {
                          var element = saleData[index];
                          return _buildItemCard(
                            element.imgUrl,
                            element.title,
                            element.quantity,
                            element.sellPrice,
                            element.date,
                            element.totalPrice,
                            element.measureUnit,
                          );
                        },
                      )
                    : Center(
                        child: Text("Sale Data is empty"),
                      )
                : Container(
                    width: 50,
                    height: 50,
                    alignment: Alignment.center,
                    child: CircularProgressIndicator(),
                  ),
          ),
        ),
      ],
    );
  }

  Widget _buildDatePick() {
    return Column(
      children: <Widget>[
        Expanded(
          flex: 2,
          child: Container(
            padding: EdgeInsets.all(10),
            color: Colors.blue[50],
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "Today",
                      style: TextStyle(fontSize: 12),
                    ),
                    Text(
                      "March 2019",
                      style: TextStyle(fontSize: 12),
                    ),
                    Text(
                      "Today",
                      style: TextStyle(color: Colors.transparent),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    _buildDateCircle("M", "4", Colors.grey[300]),
                    _buildDateCircle("T", "5", Colors.grey[300]),
                    _buildDateCircle("W", "6", Colors.grey[300]),
                    _buildDateCircle("Th", "7", Colors.grey[300]),
                    _buildDateCircle("F", "8", Colors.grey[300]),
                    _buildDateCircle("S", "9", Colors.blue[300]),
                    _buildDateCircle("Su", "10", Colors.grey[300]),
                  ],
                )
              ],
            ),
          ),
        ),
        Expanded(
          flex: 1,
          child: Container(
            margin: EdgeInsets.only(top: 5, bottom: 5),
            padding: EdgeInsets.only(left: 10, right: 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  height: 2,
                  color: Colors.black,
                ),
                SizedBox(
                  height: 5,
                ),
                Text(
                  "Last sale transactions",
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 18,
                      fontWeight: FontWeight.w400),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildDateCircle(String date, String number, Color color) {
    return Column(
      children: <Widget>[
        Text(date),
        CircleAvatar(
          backgroundColor: color,
          foregroundColor: Colors.black,
          child: Text(
            number,
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildItemCard(
      image, title, qty, price, date, totalPrice, measureUnit) {
    var token = Data.token;
    return Container(
      // padding: EdgeInsets.all(10),
      padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
      margin: EdgeInsets.all(10),
      decoration: BoxDecoration(
        boxShadow: <BoxShadow>[
          BoxShadow(
            offset: Offset(0, 3),
            color: Colors.black12,
            blurRadius: 6,
          )
        ],
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      // color: Colors.white,
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Padding(
              padding: EdgeInsets.only(right: 5),
              child: CachedNetworkImage(
                imageUrl: "http://localhost:8000/api/product/image/$image",
                httpHeaders: {
                  HttpHeaders.contentTypeHeader:
                      "application/x-www-form-urlencoded",
                  "X-Requested-With": "XMLHttpRequest",
                  HttpHeaders.authorizationHeader: "Bearer $token",
                },
                placeholder: (context, url) => new CircularProgressIndicator(),
                errorWidget: (context, url, error) => new Icon(Icons.error),
                width: 100,
                height: 100,
              ),
            ),
          ),
          Expanded(
            flex: 3,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  height: 90,
                  width: 1,
                  color: Colors.black,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      child: Text(
                        title,
                        style: TextStyle(
                            color: Colors.blue[900],
                            fontSize: 22,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 5, bottom: 5),
                      child: Row(
                        children: <Widget>[
                          Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "Quantity:",
                                  style: TextStyle(
                                    color: Colors.blue[900],
                                  ),
                                ),
                                Text(
                                  "Sell out Price:",
                                  style: TextStyle(
                                    color: Colors.blue[900],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            width: 30,
                          ),
                          Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "$qty $measureUnit",
                                  style: TextStyle(color: Colors.orange[800]),
                                ),
                                Text(
                                  "\$ $price",
                                  style: TextStyle(color: Colors.orange[800]),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      child: Row(
                        children: <Widget>[
                          Text(
                            "Date:",
                            style: TextStyle(
                              color: Colors.blue[900],
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            date,
                            style: TextStyle(color: Colors.orange[800]),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Container(
                  height: 90,
                  width: 1,
                  color: Colors.black,
                ),
              ],
            ),
          ),
          Expanded(
              flex: 1,
              child: Container(
                child: Text(
                  "\$$totalPrice",
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.blue[700]),
                ),
              )),
        ],
      ),
    );
  }
}
